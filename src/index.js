import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
//import App from './App';
import * as serviceWorker from './serviceWorker';

import { Route, BrowserRouter as Router } from 'react-router-dom'
//import Loginform from './component/Loginform';
import Registration from './component/registration'
import Registrationsucc from './component/registrationsucc'
import Fertilizer from './component/fertilizerhomepage'
import header from './component/header'

const routing= (
  <Router>
   <div>
     <Route exact  path="/" component={header}/>
     <Route path="/fertilizerhomepage" component={Fertilizer}/>
     {/* <Route path="/loginendpage" component={Loginendpage}/> */}
     <Route path="/registration" component={Registration}/>
     <Route path="/registrationsucc" component={Registrationsucc}/>

     
   </div>
  </Router>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
