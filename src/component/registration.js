import React, {Component} from 'react';
import axios from "axios";
import '../css/registration.css'
class Registration extends Component
{
    constructor(props)
    {
        super(props)
        this.state={
            Firstname:'',
            Lastname:'',
            Email:'',
            Password:'',
            Mobile:'',
            Pincode:'',
            Location:''

        }
        this.onsubmit=this.onsubmit.bind(this)
       
    }
   
    // onsubmit()
    // {
    //     this.setState({
    //         Firstname:document.getElementById('fst').value,
    //         Lastname:document.getElementById('lst').value,
    //         Email:document.getElementById('eml').value,
    //         Mobile:document.getElementById('mobilenum').value,
    //         Pincode:document.getElementById('pincode').value,
    //         Location:document.getElementById('loc').value
    //     },()=>{
    //         this.props.history.push({pathname: '/registrationsucc',
       
    //         state: { Name:this.state.Firstname,Email:this.state.Email }})
    //     })
        
      
    //   //  document.write('your info',this.state.Firstname)
        
    // }

    onsubmit()
    {
       
        var dataobj={
                    firstName:document.getElementById('fst').value,
                    lastName:document.getElementById('lst').value,
                    email:document.getElementById('eml').value,
                    password:document.getElementById('pwd').value,
                    mobile:document.getElementById('mobilenum').value,
                    pincode:document.getElementById('pincode').value,
                    location:document.getElementById('loc').value
        }

        axios.post('http://localhost:3005/registration', dataobj)
       .then(response => {
        //console.log(response,'hello')
       
        
    })  
    this.props.history.push({pathname: '/registrationsucc',
    state: {email:dataobj.email}})  
    }
  
    render()
    {
        return(
           <div className="container">
               <div className="row">
                   <div className="form_bg">
                       <form onSubmit={this.onsubmit}>
                       <h2 class="text-center">Registration</h2>
                        <br/>
                        <div class="form-group">
                            <input type="text" class="form-control" id="fst" placeholder="enter your fastname" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="lst" placeholder="enter your lastname"/>
                        </div>

                        <div class="form-group">
                            <input type="email" class="form-control" id="eml" placeholder="enter your email"/>
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control" id="pwd" placeholder="enter your password"/>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" id="mobilenum" placeholder="enter your mobile number"></input>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="pincode" placeholder="enter your pincode"></input>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="loc" placeholder="enter your location">

                            </input>
                        </div>
                        <div class="align-center">
                            <button type="submit" class="btn btn-secondary">Registration</button>
                        </div>
                        <div class="alrdacc">Already have account?<span><a href="/">Login</a></span></div>
                       </form>
                   </div>
               </div>
           </div>
        )
    }
}
export default Registration