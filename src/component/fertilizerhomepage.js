import React, {Component} from 'react'
import '../css/fertilizerhomepage.css'
import Header from './header'
class Fertilizer extends Component 
{
   render()
   {
       return(
           <div>
                <Header/>
              <div className="col-md-12 fermaindiv">
               <div className="col-md-4 imgpadding">
                <img  alt="firstimg" src="https://res.cloudinary.com/dwbsz6uqk/image/upload/c_scale,h_260/v1577770067/outlook-for-the-global-fertilizer-market_rkm2jw.jpg"></img>

                <div className="imgcontent">
                    <p>To germinate, rice seeds need to absorb a certain amount of water and be exposed to a temperature range of 10–40 °C. This breaks the dormancy stage of the seed.</p>
                </div>
               </div>
               <div className="col-md-4 imgpadding">
                    <img alt="" src="https://res.cloudinary.com/dwbsz6uqk/image/upload/c_scale,h_260,w_392/v1577770057/jpeg_n49wpt.jpg"></img>
                    <div className="imgcontent">
                        <p>Foliar feeding is a ‘by-pass’ approach, overtaking conventional soil applied fertilizer whenever it does not perform well enough. Foliar application overcomes soil fertilization limitations like leaching, insoluble fertilizer precipitation,</p>
                    </div>
               </div>
               <div className="col-md-4 imgpadding">
                <img alt="" src="https://res.cloudinary.com/dwbsz6uqk/image/upload/c_scale,h_260,w_392/v1577776261/paddyside-500x500_bwwczs.jpg"></img>
                <div className="imgcontent">
                Important management factors should be considered during the growth of the rice crop. These include planting method, water, fertilizer, weeds, and pests and diseases.
                </div>
               </div>
              </div>
              </div>
           
               
          
       )
   }
}
export default Fertilizer